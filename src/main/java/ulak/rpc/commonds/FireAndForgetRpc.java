package ulak.rpc.commonds;

public interface FireAndForgetRpc<T> extends RpcCommand {

    void apply(T data);

    default Class<?> getType() {
        return FireAndForgetRpc.class;
    }
}
