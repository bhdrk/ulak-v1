package ulak.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ConfigFactory {

    private final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    public UlakConfig read() throws IOException {
        UlakConfig config = new UlakConfig();

        config = readFromResources(config);
        config = readFromSystemProp(config);

        return config;
    }

    private UlakConfig readFromSystemProp(UlakConfig config) throws IOException {
        var configProperty = System.getProperty("ulak.config");
        if (configProperty != null) {
            var configPath = Path.of(configProperty);

            if (Files.exists(configPath)) {
                try (var is = Files.newInputStream(configPath)) {
                    config = mapper.readerForUpdating(config).readValue(is);
                }
            }
        }
        return config;
    }

    private UlakConfig readFromResources(UlakConfig config) throws IOException {
        var resource = getClass().getResource("/ulak.yaml");
        if (resource != null) {
            try (var is = resource.openStream()) {
                config = mapper.readerForUpdating(config).readValue(is);
            }
        }
        return config;
    }
}
