package ulak;

import com.fasterxml.jackson.databind.ObjectMapper;
import ulak.config.ConfigFactory;
import ulak.config.UlakConfig;
import ulak.json.JsonDecoder;
import ulak.json.JsonEncoder;
import ulak.rpc.RpcRouter;
import ulak.rpc.RpcSchema;
import ulak.rsocket.TransportClientFactory;
import ulak.rsocket.TransportServer;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class UlakContext extends AbstractContext {

    public CompletableFuture<UlakConfig> ulakConfig() {
        return single("ulakConfig", () -> async(() -> {
            try {
                return new ConfigFactory().read();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }));
    }

    public CompletableFuture<RpcRouter> rpcRouter() {
        return single("rpcRouter", () -> completed(new RpcRouter()));
    }


    public CompletableFuture<ObjectMapper> objectMapper() {
        return single("jsonObjectMapper", () -> completed(new ObjectMapper()));
    }

    public CompletableFuture<Encoder> encoder() {
        return single("jsonEncoder",
                () -> objectMapper().thenApply(JsonEncoder::new)
        );
    }

    public CompletableFuture<Decoder> decoder() {
        return single("jsonDecoder",
                () -> objectMapper().thenApply(JsonDecoder::new)
        );
    }

    public CompletableFuture<RpcSchema> rpcSchema() {
        return single("rpcSchema",
                () -> newFuture(RpcSchema.builder())
                        .thenCombine(encoder(), RpcSchema.RpcSchemaBuilder::encoder)
                        .thenCombine(decoder(), RpcSchema.RpcSchemaBuilder::decoder)
                        .thenApply(RpcSchema.RpcSchemaBuilder::build)
        );
    }

    public CompletableFuture<TransportServer> transportServer() {
        return single("transportServer",
                () -> ulakConfig().thenApply(config -> TransportServer.builder().config(config.getServer()))
                        .thenCombine(rpcRouter(), TransportServer.TransportServerBuilder::rpcRouter)
                        .thenCombine(rpcSchema(), TransportServer.TransportServerBuilder::rpcSchema)
                        .thenApply(TransportServer.TransportServerBuilder::build)
        );
    }

    public CompletableFuture<TransportClientFactory> discoveryService() {
        return single("discoveryService", () -> newFuture(TransportClientFactory.builder())
                .thenApply(TransportClientFactory.TransportClientFactoryBuilder::build));
    }
}
