package ulak.config;

import lombok.Data;

@Data
public class TransportServerConfig {
    private String host;
    private int port;
}