package ulak.rpc.commonds;

import java.util.stream.Stream;

public interface RequestStreamRpc<T, R> {

    Stream<R> apply(T input);

    default Class<?> getType() {
        return RequestStreamRpc.class;
    }
}
