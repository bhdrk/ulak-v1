package ulak.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ulak.Encoder;

import java.io.IOException;
import java.nio.ByteBuffer;

public class JsonEncoder implements Encoder {

    private final ObjectMapper mapper;

    public JsonEncoder(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ByteBuffer encode(Object input) throws IOException {
        return ByteBuffer.wrap(mapper.writeValueAsBytes(input));
    }
}
