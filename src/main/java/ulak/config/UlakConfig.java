package ulak.config;

import lombok.Data;

@Data
public class UlakConfig {

    private TransportServerConfig server;

}
