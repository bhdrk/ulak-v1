package ulak;

public class Ulak extends Thread {


    public void run() {
        for (int i = 0; i < 3; i++)
            System.out.println(Thread.currentThread().getName() + " called from run");
    }

    public static void main(String[] args) {
        Ulak t1 = new Ulak();
        Ulak t2 = new Ulak();
        // this will call run() method
        t1.start();
        t2.start();
        for (int i = 0; i < 3; i++) {
            // Control passes to child thread
            Thread.yield();
            System.out.println(Thread.currentThread().getName() + " called from main");
        }
    }
}