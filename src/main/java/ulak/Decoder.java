package ulak;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface Decoder {
    <T> T decode(ByteBuffer input, Class<T> type) throws IOException;

    default <T> T tryDecode(ByteBuffer input, Class<T> type) {
        try {
            return decode(input, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
