package ulak.rpc;

import ulak.rpc.commonds.RpcCommand;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class RpcRouter {

    private final ConcurrentMap<String, RpcCommand> registry;

    public RpcRouter() {
        registry = new ConcurrentHashMap<>();
    }

    public void register(RpcCommand command) {
        registry.put(command.getRoute(), command);
    }

    public RpcCommand get(String route) {
        return registry.get(route);
    }
}
