package ulak.rsocket;

import io.rsocket.core.RSocketConnector;
import io.rsocket.transport.netty.client.TcpClientTransport;
import lombok.Builder;
import ulak.config.TransportClientConfig;

public class TransportClientFactory {


    private TransportClientConfig config;

    @Builder
    public TransportClientFactory(TransportClientConfig config) {
        this.config = config;
    }

    public TransportClientFactory connect() {
        var socket = RSocketConnector.connectWith(TcpClientTransport.create(config.getHost(), config.getPort()))
                .block();
        return this;

    }
}
