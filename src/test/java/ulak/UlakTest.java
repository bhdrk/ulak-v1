package ulak;

import org.testng.annotations.Test;

public class UlakTest {

    @Test
    public void testServer() {
//        var serverConfig = new TransportServerConfig("localhost", 7000);
//        var server = new TransportServer(serverConfig);
//        server.start().waitFor();
    }

    @Test
    public void testClient() {
        var clientConfig = new UlakClientConfig("localhost", 7000);

        var client = new UlakClient(clientConfig);
        client.connect().send("Hello").subscribe(p -> {
            System.out.println(p.getDataUtf8());
        });

        client.onClose().block();
    }
}
