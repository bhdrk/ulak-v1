package ulak;

import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketConnector;
import io.rsocket.transport.netty.client.TcpClientTransport;
import io.rsocket.util.DefaultPayload;
import reactor.core.publisher.Mono;

import java.io.Closeable;

public class UlakClient implements Closeable {

    private UlakClientConfig config;
    private RSocket socket;

    public UlakClient(UlakClientConfig config) {
        this.config = config;
    }

    public UlakClient connect() {
        var transport = TcpClientTransport.create(config.getHost(), config.getPort());
        this.socket = RSocketConnector.connectWith(transport).block();
        return this;
    }

    public Mono<Payload> send(String data) {
        return socket.requestResponse(DefaultPayload.create(data));
    }

    public Mono<Void> onClose() {
        return socket.onClose();
    }

    @Override
    public void close() {
        socket.dispose();
    }
}
