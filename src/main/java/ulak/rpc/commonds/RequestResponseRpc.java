package ulak.rpc.commonds;

public interface RequestResponseRpc<T, R> extends RpcCommand {

    R apply(T input);

    @Override
    default Class<?> getType() {
        return RequestResponseRpc.class;
    }
}
