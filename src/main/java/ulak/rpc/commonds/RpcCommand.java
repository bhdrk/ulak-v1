package ulak.rpc.commonds;

public interface RpcCommand {

    Class<?> getType();

    String getRoute();
}
