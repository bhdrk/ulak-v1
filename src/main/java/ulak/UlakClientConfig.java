package ulak;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class UlakClientConfig {
    private final String host;
    private final int port;

    public UlakClientConfig(String host, int port) {
        this.host = host;
        this.port = port;
    }
}
