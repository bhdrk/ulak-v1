package ulak.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ulak.Decoder;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class JsonDecoder implements Decoder {

    private final ObjectMapper mapper;

    public JsonDecoder(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public <T> T decode(ByteBuffer input, Class<T> type) throws IOException {
        return mapper.readValue(new ByteBufferBackedInputStream(input), type);
    }

    static class ByteBufferBackedInputStream extends InputStream {

        ByteBuffer buf;

        public ByteBufferBackedInputStream(ByteBuffer buf) {
            this.buf = buf;
        }

        public int read() throws IOException {
            if (!buf.hasRemaining()) {
                return -1;
            }
            return buf.get() & 0xFF;
        }

        public int read(byte[] bytes, int off, int len) throws IOException {
            if (!buf.hasRemaining()) {
                return -1;
            }
            len = Math.min(len, buf.remaining());
            buf.get(bytes, off, len);
            return len;
        }
    }
}
