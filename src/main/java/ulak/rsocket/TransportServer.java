package ulak.rsocket;

import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.SocketAcceptor;
import io.rsocket.core.RSocketServer;
import io.rsocket.frame.decoder.PayloadDecoder;
import io.rsocket.transport.netty.server.CloseableChannel;
import io.rsocket.transport.netty.server.TcpServerTransport;
import io.rsocket.util.DefaultPayload;
import lombok.Builder;
import net.jodah.typetools.TypeResolver;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ulak.config.TransportServerConfig;
import ulak.rpc.RpcMetadata;
import ulak.rpc.RpcRouter;
import ulak.rpc.RpcSchema;
import ulak.rpc.commonds.FireAndForgetRpc;
import ulak.rpc.commonds.RequestResponseRpc;
import ulak.rpc.commonds.RequestStreamRpc;

import java.io.Closeable;
import java.nio.ByteBuffer;
import java.util.function.Function;

public class TransportServer implements Closeable {

    private final TransportServerConfig config;

    private final RpcRouter rpcRouter;

    private final RpcSchema rpcSchema;

    private CloseableChannel channel;

    @Builder
    private TransportServer(TransportServerConfig config, RpcRouter rpcRouter, RpcSchema rpcSchema) {
        this.config = config;
        this.rpcRouter = rpcRouter;
        this.rpcSchema = rpcSchema;
    }

    public TransportServer start() {

        System.out.println("Server is starting...");
        var serverSocket = createSocket();
        this.channel = createServer(serverSocket);
        System.out.println("Server started!");
        return this;
    }

    private CloseableChannel createServer(RSocket serverSocket) {
        return RSocketServer.create(SocketAcceptor.with(serverSocket))
                .payloadDecoder(PayloadDecoder.ZERO_COPY)
                .bind(TcpServerTransport.create(config.getHost(), config.getPort()))
                .block();
    }

    public void waitFor() {
        if (channel != null) {
            channel.onClose().block();
            System.out.println("Server stopped!");
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private RSocket createSocket() {
        return new RSocket() {
            @Override
            public Mono<Void> fireAndForget(Payload payload) {
                var decoder = rpcSchema.getDecoder();

                var metadata = decoder.tryDecode(payload.getMetadata(), RpcMetadata.class);
                var command = rpcRouter.get(metadata.getRoute());

                if (FireAndForgetRpc.class.equals(command.getType()) && command instanceof FireAndForgetRpc) {
                    var inputType = TypeResolver.resolveRawArgument(FireAndForgetRpc.class, command.getClass());
                    var data = decoder.tryDecode(payload.getData(), inputType);
                    ((FireAndForgetRpc) command).apply(data);
                    return Mono.empty();
                } else {
                    return Mono.error(new RuntimeException("Unsupported command type"));
                }
            }

            @Override
            public Flux<Payload> requestStream(Payload payload) {
                var encoder = rpcSchema.getEncoder();
                var decoder = rpcSchema.getDecoder();

                var metadata = decoder.tryDecode(payload.getMetadata(), RpcMetadata.class);
                var command = rpcRouter.get(metadata.getRoute());

                if (RequestStreamRpc.class.equals(command.getType()) && command instanceof RequestStreamRpc) {
                    var inputType = TypeResolver.resolveRawArgument(RequestStreamRpc.class, command.getClass());
                    var data = decoder.tryDecode(payload.getData(), inputType);
                    var output = ((RequestStreamRpc) command).apply(data);
                    return Flux.fromStream(output)
                            .map((Function<Object, ByteBuffer>) encoder::tryEncode)
                            .map((Function<ByteBuffer, Payload>) DefaultPayload::create).onBackpressureBuffer();
                } else {
                    return Flux.error(new RuntimeException("Unsupported command type"));
                }
            }

            @Override
            public Mono<Payload> requestResponse(Payload payload) {
                try {
                    var encoder = rpcSchema.getEncoder();
                    var decoder = rpcSchema.getDecoder();

                    var metadata = decoder.tryDecode(payload.getMetadata(), RpcMetadata.class);
                    var command = rpcRouter.get(metadata.getRoute());

                    if (RequestResponseRpc.class.equals(command.getType()) && command instanceof RequestResponseRpc) {
                        var inputType = TypeResolver.resolveRawArgument(RequestResponseRpc.class, command.getClass());
                        var data = decoder.tryDecode(payload.getData(), inputType);
                        var output = ((RequestResponseRpc) command).apply(data);
                        return Mono.just(output)
                                .map(encoder::tryEncode)
                                .map(DefaultPayload::create);
                    } else {
                        return Mono.error(new RuntimeException("Unsupported command type"));
                    }
                } catch (Throwable t) {
                    return Mono.error(t);
                }
            }

            @Override
            public Flux<Payload> requestChannel(Publisher<Payload> payloads) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public void close() {
        System.out.println("Server is stopping...");
        channel.dispose();
    }
}

