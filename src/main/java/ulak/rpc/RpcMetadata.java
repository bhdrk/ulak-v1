package ulak.rpc;

import lombok.Data;

import java.io.Serializable;

@Data
public class RpcMetadata implements Serializable {

    public static final String VERSION = "1";

    private String version;
    private String route;
}
