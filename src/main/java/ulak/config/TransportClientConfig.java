package ulak.config;

import lombok.Data;

@Data
public class TransportClientConfig {
    private String host;
    private int port;
}
