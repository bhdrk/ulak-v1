package ulak.rsocket;

import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.util.DefaultPayload;
import lombok.Builder;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Mono;
import ulak.rpc.RpcMetadata;
import ulak.rpc.RpcSchema;

import java.util.concurrent.CompletableFuture;

public class TransportClient {

    private final RSocket socket;

    private final RpcSchema rpcSchema;

    @Builder
    public TransportClient(RSocket socket, RpcSchema rpcSchema) {
        this.socket = socket;
        this.rpcSchema = rpcSchema;
    }

    public void fireAndForget(String route, Object input) { // client.fireAndForget("/append-entries")
        var rpcMetadata = new RpcMetadata();
        rpcMetadata.setRoute(route);
        rpcMetadata.setVersion(RpcMetadata.VERSION);

        var payload = createPayload(input, rpcMetadata);

        var future = new CompletableFuture<Void>();
        socket.fireAndForget(payload);

    }

    private Payload createPayload(Object input, RpcMetadata rpcMetadata) {
        var data = rpcSchema.getEncoder().tryEncode(input);
        var metadata = rpcSchema.getEncoder().tryEncode(rpcMetadata);
        return DefaultPayload.create(data, metadata);
    }

    public void requestResponse() {
        CompletableFuture<Payload> future = new CompletableFuture<>();
        socket.requestResponse(DefaultPayload.create("")).subscribe(new BaseSubscriber<Payload>() {
            @Override
            protected void hookOnNext(Payload value) {
                future.complete(value);
            }

            @Override
            protected void hookOnError(Throwable t) {
                future.completeExceptionally(t);
            }
        });
    }

    public void requestStream() {
        socket.requestStream(DefaultPayload.create("")).subscribe(new BaseSubscriber<Payload>() {

        });
    }
}
