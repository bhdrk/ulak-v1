package ulak.rpc.commonds;

import java.util.stream.Stream;

public interface RequestChannelRpc<T, R> {

    Stream<R> apply(Stream<T> input);

    default Class<?> getType() {
        return RequestChannelRpc.class;
    }
}
