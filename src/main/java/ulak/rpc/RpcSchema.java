package ulak.rpc;

import lombok.Builder;
import ulak.Decoder;
import ulak.Encoder;

public class RpcSchema {

    private final Encoder encoder;
    private final Decoder decoder;

    @Builder
    public RpcSchema(Encoder encoder, Decoder decoder) {
        this.encoder = encoder;
        this.decoder = decoder;
    }

    public Encoder getEncoder() {
        return encoder;
    }

    public Decoder getDecoder() {
        return decoder;
    }
}
