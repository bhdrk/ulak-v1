package ulak;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface Encoder {
    ByteBuffer encode(Object input) throws IOException;

    default ByteBuffer tryEncode(Object input) {
        try {
            return encode(input);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
